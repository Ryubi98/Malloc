#!/bin/sh

separation()
{
    echo ""
    echo "===================="
    echo ""
}

check()
{
    echo -n $function - 
    if [ $res -ne 0 ]; then
        echo " ERROR"
    else
        echo " SUCCESS"
    fi
}

echo "TESTSUITE FOR MALLOC"

while IFS='\n' read -r function; do
    separation
    LD_PRELOAD=./libmalloc.so $function >> output.txt
    res=$(echo $?)
    check
done < $PWD/tests/commands.txt

separation
