#include <stdio.h>

#include "../src/allocation.h"

int main(void)
{
    void *ptr = malloc(sizeof(int) * 5);
    void *second = malloc(sizeof(int) * 5);

    free(ptr);

    int *p = malloc(sizeof(int) * 6);

    free(p);
    free(second);

    char *str = calloc(30, sizeof(char));

    for (size_t i = 0; i < 26; i++)
        str[i] = 'a' + i;
    str[26] = '\0';

    printf("%s\n", str);

    printf("MAIN.C - SUCCES\n");

    return 0;
}
