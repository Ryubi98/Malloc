#include "allocation.h"

__attribute__((__visibility__("default")))
void *calloc(size_t nmemb, size_t size)
{
    char *ptr = realloc(NULL, nmemb * size);
    if (!ptr)
        return NULL;

    for (size_t i = 0; i < nmemb * size; i++)
        ptr[i] = 0;

    return ptr;
}

