#ifndef ALLOCATION_H
#define ALLOCATION_H

#include <stddef.h>

struct metadata
{
    size_t size_block;
    size_t free;
    struct metadata *p_prev;
};

void *realloc(void *ptr, size_t size);

void *malloc(size_t size);

void *calloc(size_t nmemb, size_t size);

void free(void *ptr);

#endif /* ! ALLOCATION_H */
