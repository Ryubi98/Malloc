#define _GNU_SOURCE

#include <sys/mman.h>

#include "allocation.h"

static char **init_global()
{
    char **map = mmap(NULL, 4096, PROT_READ | PROT_WRITE,
                      MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (!map)
        return NULL;

    *map = NULL;

    return map;
}

static void *init(size_t *size_map)
{
    struct metadata *md = mmap(NULL, 4096, PROT_READ | PROT_WRITE,
                               MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (md == MAP_FAILED)
        return NULL;

    struct metadata new =
    {
        .size_block = 4096 - sizeof(struct metadata),
        .free = 1,
        .p_prev = NULL
    };

    *md = new;
    *size_map = 4096;

    return md;
}

static void *resize(struct metadata *map, size_t *size_map, size_t new_size)
{
    char *new = mremap(map, *size_map, new_size, MREMAP_MAYMOVE, NULL);
    if (new == MAP_FAILED)
        return map;

    void *ptr = new;
    char *cpy = ptr;
    map = ptr;
    while (cpy + map->size_block + sizeof(struct metadata) < new + *size_map)
    {
        ptr = cpy + map->size_block + sizeof(struct metadata);
        map = ptr;
        cpy = ptr;
    }

    if (map->free)
        map->size_block += (new_size - *size_map);
    else
    {
        struct metadata next =
        {
            .size_block = (new_size - *size_map),
            .free = 1,
            .p_prev = map
        };
        ptr = new + *size_map;
        map = ptr;
        *map = next;
    }

    *size_map = new_size;

    return new;
}

static void release_element(char *map, size_t size_map, char *block)
{
    if (!block)
        return;

    void *ptr = block - sizeof(struct metadata);
    struct metadata *md = ptr;

    if (block + md->size_block < map + size_map)
    {
        ptr = block + md->size_block;
        struct metadata *next =  ptr;
        if (next->free)
            md->size_block += next->size_block + sizeof(struct metadata);
    }

    if (md->p_prev && md->p_prev->free)
        md->p_prev->size_block += md->size_block + sizeof(struct metadata);

    md->free = 1;
}

static void release_map(char **map, size_t *size_map, size_t *nb_elements)
{
    munmap(*map, *size_map);
    *map = NULL;
    *size_map = 0;
    *nb_elements = 0;
}

static void realloc_free(char **map, size_t *size_map, size_t *nb_elements,
                         void *ptr)
{
    if (!*size_map)
        return;
    else if (*nb_elements == 1)
        release_map(map, size_map, nb_elements);
    else
    {
        release_element(*map, *size_map, ptr);
        *nb_elements -= 1;
    }
}

static void add_metadata(struct metadata *md, size_t size)
{
    struct metadata new =
    {
        .size_block = md->size_block - size - sizeof(struct metadata),
        .free = 1,
        .p_prev = md
    };

    md->size_block = size;
    md->free = 0;
    void *ptr = md;
    char *cpy = ptr;
    cpy += md->size_block + sizeof(struct metadata);
    ptr = cpy;
    md = ptr;
    *md = new;

}

static void *allocate(struct metadata *map, size_t *size_map, size_t size)
{
    int found_free = 0;
    struct metadata *md = map;
    void *ptr = md;
    char *cpy = ptr;

    while (!found_free)
    {
        if (md->free)
        {
            if (md->size_block >= size + sizeof(struct metadata) + 1)
            {
                found_free = 1;
                break;
            }
        }

        ptr = map;
        char *start = ptr;
        if(cpy + md->size_block + sizeof(struct metadata) < start + *size_map)
        {
            ptr = cpy + md->size_block + sizeof(struct metadata);
            md = ptr;
            cpy = ptr;
        }
        else
            break;
    }

    if (found_free)
    {
        add_metadata(md, size);
        return md + 1;
    }

    map = resize(map, size_map, *size_map + (2 * size));
    if (!map)
        return NULL;
    return allocate(map, size_map, size);
}

static void *realloc_resize(void *map, char *ptr, size_t *size_map,
                            size_t size)
{
    void *cpy = ptr - sizeof(struct metadata);
    struct metadata *md = cpy;
    if (md->size_block == size)
        return ptr;
    else if (md->size_block < size)
    {
        char *new = allocate(map, size_map, size);
        if (!new)
            return ptr;
        for (size_t i = 0; i < md->size_block; i++)
            new[i] = ptr[i];
        release_element(map, *size_map, ptr);
        return new;
    }
    else if (md->size_block > size + sizeof(struct metadata))
    {
        add_metadata(md, size);
        return ptr;
    }

    return ptr;
}

__attribute__((__visibility__("default")))
void *realloc(void *ptr, size_t size)
{
    static char **map_global = NULL;
    static size_t size_map = 0;
    static size_t nb_elements = 0;

    if (!map_global)
    {
        map_global = init_global();
        if (!map_global)
            return NULL;
    }

    void *map = *map_global;

    if (!size)
    {
        char *cpy_map = map;
        realloc_free(&cpy_map, &size_map, &nb_elements, ptr);
        *map_global = cpy_map;
        return NULL;
    }

    if (!map)
    {
        map = init(&size_map);
        if (!map)
            return NULL;
        *map_global = map;
    }

    if (ptr)
        return realloc_resize(map, ptr, &size_map, size);

    nb_elements += 1;
    return allocate(map, &size_map, size);
}
