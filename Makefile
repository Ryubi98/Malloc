CC ?= gcc
CFLAGS = -Wall -Wextra -Werror -pedantic -std=c99 -fPIC\
         -fvisibility=hidden -fno-builtin -g
LDFLAGS = -shared

TARGET_LIB = libmalloc.so

SRC = $(wildcard src/*.c)
OBJS = $(SRC:.c=.o)


.PHONY: all ${TARGET_LIB} clean

${TARGET_LIB}: ${OBJS}
	${CC} -g ${LDFLAGS} -o $@ $^

all: ${TARGET_LIB}

check: all
	$(CC) -g -o main tests/main.c -L. -lmalloc
	./tests/test.sh
	LD_LIBRARY_PATH=. ./main

clean:
	${RM} ${TARGET_LIB} ${OBJS} *.txt *.tar main
